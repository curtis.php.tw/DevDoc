Docker 安裝 DB2
======


Docker Hub
------
<https://hub.docker.com/r/ibmcom/db2>

<br>

Docker 映像
------
### 拉取 db2 映像

    docker pull ibmcom/db2

### 檢查映像

    docker images


### 檢查 db2 映像

    docker images | grep db2

Output:
```
REPOSITORY                       TAG           IMAGE ID       CREATED        SIZE
ibmcom/db2                       latest        a6a5ee354fb1   2 months ago   2.95GB
```

### 刪除映像

    docker rmi ${IMAGE ID}
    docker rmi a6a5ee354fb1

<br>

Docker 容器
------
### 運行 db2 容器

**Sample**

    docker run -itd --name mydb2 --privileged=true \
    -p 50000:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=<choose an instance password> -e DBNAME=testdb \
    -v <db storage dir>:/database ibmcom/db2
    

**Local**

    docker run -itd --name sql3 --privileged=true \
    -p 50000:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=Dev127336 -e DBNAME=DevDb \
    -v /Users/Enoxs/Mac_Document/Tools/DevTools/ibmcom/db2:/database ibmcom/db2


**參數功能：**

+ run : docker 建立 container 並且執行的指令
+ -itd : i - 交互模式、t - 分配一個偽終端機、d - 背景執行
+ --name : 指定容器名稱為 sql3
+ --privileged = true : 允許特權容器，可以訪問主機
+ -p 50000:50000 : 將容器的 50000 端口映射到主機的 50000 端口。
+ -e DB2INST1_PASSWORD=Dev127336 : 初始化 db2inst1 用戶的密碼為 Dev127336。
+ -e DBNAME=DevDb : 初始化資料庫為 DevDb
+ -v : 掛載本機端位置 

### 檢查容器 (運行中)

    docker ps

Output:
```
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                                  NAMES
925859fd62b1   mysql:8   "docker-entrypoint.s…"   5 seconds ago   Up 4 seconds   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp   sql2
```

### 停止容器

    docker stop sql3

### 檢查容器 (全部)

    docker ps -a

Output:
```
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS                       PORTS     NAMES
115863d85607   mysql:8   "docker-entrypoint.s…"   24 seconds ago   Exited (137) 2 seconds ago             sql2
```

### 啟動容器

    docker start sql3

### 進入容器

**Sample**

    docker exec -ti mydb2 bash -c "su - ${DB2INSTANCE}"

**Local**

    docker exec -ti sql3 bash -c "su - db2inst1"

### 退出容器

    exit

### 刪除容器

    docker rm sql3

<br>

進入容器
------
`進入到 Linux 的操作環境`

**Sample**

    docker exec -ti mydb2 bash -c "su - ${DB2INSTANCE}"

**Local**

    docker exec -ti sql3 bash -c "su - db2inst1"


### 指令測試
```
$> su - db2inst1
$> db2start
$> db2sampl
$> db2 connect to SAMPLE
$> db2 "SELECT * FROM STAFF"
```

<br>


### 退出容器

    logout


Dbeaver
------
+ Host : localhost
+ Port : 50000
+ Database : SAMPLE
+ Username : db2inst1
+ Password : Dev127336


### DB2 Driver
<https://mvnrepository.com/artifact/com.ibm.db2.jcc/db2jcc/db2jcc4>













